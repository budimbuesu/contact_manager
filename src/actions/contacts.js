import * as constants from './constants'

export const loadContacts = () => {
	//to do request getContacts
}

export const setContactsRequested = () => {
  return {
    type: constants.SET_CONTACTS_REQUESTED
  }
}

export const setContactsSucceeded = (contacts) => {
    return {
      type: constants.SET_CONTACTS_SUCCEEDED,
      payload: {
        contacts
      }
    }
}

export const setContactsError = (error) => {
  return {
    type: constants.SET_CONTACTS_FAILED,
    payload: {
      error
    }
  }
}

export const createNewContact = (contact) => {
  //to do request NewContact
}

export const addContactToStore = (contact) => {
  return {
    type: constants.ADD_CONTACT_TO_STORE,
    payload: {
      contact
    }
  }
}

export const addContactError = (error) => {
  return {
    type: constants.ADD_CONTACT_FAILED,
    payload: {
      error
    }
  }
}
